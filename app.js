const CONFIG = require('./config/CONFIG')

const express = require('express')
const app = express()
const bodyParser = require('body-parser')


const mongo = require('mongodb').MongoClient
const DB_PATH = 'mongodb://localhost:27017/blog'

const blogRouter = require('./router/blog-router')
const uaaRouter = require('./router/uaa-router')
const fileRouter = require('./router/file-router')

// 设置静态文件目录
app.use(express.static('static'))


// 解析req.body
app.use(bodyParser.json())
// 设置跨域相关
app.all('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Content-Type,Content-Length, Authorization, Accept,X-Requested-With')
  res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS')
  res.header('X-Powered-By', '3.2.1')
  if (req.method == 'OPTIONS') res.send(200)
  else  next()
})

// 路由
blogRouter(app, mongo, DB_PATH)
uaaRouter(app, mongo, DB_PATH)
fileRouter(app, mongo, DB_PATH)

// 创建server 监听端口
const Server = app.listen(3000, () => {
  const port = Server.address().port

  console.log(`Express Server is listen at port: ${port}.`)
})