const moment = require('moment')
const checkAuth = require('../utils/function').checkAuth
const MongoDB = require('mongodb')
const ObjectId = MongoDB.ObjectID

module.exports = (app, mongo, DB_PATH) => {

  app.get('/api/comments', (req, res) => {
    console.log('/api/comments')

    mongo.connect(DB_PATH, (err, db) => {
      let comments = db.collection('comments')

      let articleId = req.query.articleId
      if (!articleId) {
        res.send({
          msg: 'lost a id of article',
          status: 400
        })
      } else {
        comments.find({ articleId: articleId }).toArray((err, cms) => {
          if (err) {
            res.send({
              msg: 'get comments in db failed',
              status: 401
            })
          } else {
            res.send({
              content: cms,
              status: 200,
              msg: 'get comments success'
            })
          }
        })
      }
    })
  })

  app.post('/api/comment', (req, res) => {
    let commentJson = req.body

    mongo.connect(DB_PATH, (err, db) => {
      let comments = db.collection('comments')

      if (commentJson.articleId && commentJson.content) {
        comments.insertOne({
          articleId: commentJson.articleId,
          userId: commentJson.userId || '',
          content: commentJson.content,
          timeTag: moment().format('YYYY/MM/DD dddd h:mm:ss a')
        }, (err, comm) => {
          if (!err) {
            res.send({
              msg: 'insert a comment success',
              status: 200
            })
          } else {
            res.send({
              msg: 'save in db failed',
              status: 401
            })
          }
        })
      } else {
        res.send({
          msg: 'you should send an id of article',
          status: 400
        })
      }
    })
  })

  app.get('/api/articles', (req, res) => {
    mongo.connect(DB_PATH, (err, db) => {
      let collection = db.collection('articles')

      collection.find({}).toArray((err, users) => {
        res.send(users)
        db.close()
      })
    })
  })

  app.post('/api/article', (req, res) => {

    /**
     * req.body
     *
     * must contain a 'token'
     * fetch from Server
     *
     * {
     *  token: 'xxx'
     * }
     */

    let articleJson = req.body
    if (articleJson.token) {
      const jwt = require('jsonwebtoken')
      const salt = require('../config/CONFIG').salt

      let checkAuth = () => {
        return new Promise((resolve, reject) => {
          let decodeUsername = ''
          jwt.verify(articleJson.token, salt, (err, decode) => {
            if (!err) {
              decodeUsername = decode.username
            }
          })
          console.log(decodeUsername)
          mongo.connect(DB_PATH, (err, db) => {
            let users = db.collection('users')

            users.find({ username: decodeUsername }).toArray((err, user) => {
              if (user.length > 0) {
                resolve('next')
              } else {
                reject('reject')
              }
              db.close()
            })
          })
        })
      }

      let insertArticle = async () => {
        try {
          let judge = await checkAuth(articleJson, mongo, DB_PATH)
          if (judge === 'next') {
            mongo.connect(DB_PATH, (err, db) => {
              let collection = db.collection('articles')

              let { token, ..._articleJson } = articleJson

                _articleJson.timeTag = moment().format('YYYY/MM/DD dddd h:mm:ss a')

              collection.insertOne(_articleJson, (err, data) => {
                if (!err) {
                  console.log('an article saved in MongoDB://articles/ at ' + moment().format('YYYY/MM/DD dddd h:mm:ss a'))
                  res.send({
                    msg: `insert an article success --title ${articleJson.title || ''}`,
                    status: 200
                  })
                } else {
                  console.error(err)
                }
              })
            })
          }
        } catch (e) {
          res.send({
            msg: `you have no rights to insert an article - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
            status: 400
          })
        }
      }

      insertArticle()

    } else {
      res.send({
        msg: 'you should send a token I give to you',
        status: 401
      })
    }
  })

  app.delete('/api/article', (req, res) => {

    /**
     * req.body
     *
     * {
     *    id: 'xxx',
     *    token: 'xxx'
     * }
     */

    let dataJson = req.body
    let checkId = () => {
      return new Promise((resolve, reject) => {
        mongo.connect(DB_PATH, (err, db) => {
          let articles = db.collection('articles')
          console.log(dataJson.id)
          let _id = ''
          if ((typeof dataJson.id === 'string' && dataJson.id.length === 24)) {
            _id = ObjectId(dataJson.id)
            articles.find({ _id: _id }).toArray((err, id) => {
              console.log('ids', id)
              if (id.length > 0) {
                resolve('next')
              } else {
                reject('reject')
              }
              db.close()
            })
          } else {
            reject('reject')
          }
        })
      })
    }
    let deleteArticle = async () => {
      let judge = await checkAuth(dataJson, mongo, DB_PATH)
      if (judge === 'next') {
        let checkIdResult = await checkId()
        if (checkIdResult === 'next') {
          mongo.connect(DB_PATH, (err, db) => {
            let articles = db.collection('articles')
            articles.deleteOne({ _id: ObjectId(dataJson.id) }, (err, article) => {
              if (!err) {
                res.send({
                  msg: `delete an article success - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
                  status: 200
                })
              } else {
                res.send({
                  msg: `delete failed by some undefined mistake - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
                  status: 400
                })
              }
            })
          })
        } else {
          res.send({
            msg: `no such id - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
            status: 401
          })
        }
      }
    }
    deleteArticle().catch((err) => {
      console.log(err)
      res.send({
        msg: `no rights to delete article, check your token or id - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
        status: 402
      })
    })

  })

  app.put('/api/article', (req, res) => {
    let dataJson = req.body
    let checkId = () => {
      return new Promise((resolve, reject) => {
        mongo.connect(DB_PATH, (err, db) => {
          let articles = db.collection('articles')
          console.log(dataJson.id)
          let _id = ''
          if ((typeof dataJson.id === 'string' && dataJson.id.length === 24)) {
            _id = ObjectId(dataJson.id)
            articles.find({ _id: _id }).toArray((err, id) => {
              console.log('ids', id)
              if (id.length > 0) {
                resolve('next')
              } else {
                reject('reject')
              }
              db.close()
            })
          } else {
            reject('reject')
          }
        })
      })
    }
    let putArticle = async () => {
      let judge = await checkAuth(dataJson, mongo, DB_PATH)
      if (judge === 'next') {
        let checkIdResult = await checkId()
        if (checkIdResult === 'next') {
          mongo.connect(DB_PATH, (err, db) => {
            let articles = db.collection('articles')
            let { id, token, ...updateJson } = dataJson
            articles.updateOne({ _id: ObjectId(dataJson.id) }, { $set: updateJson }, (err, article) => {
              if (!err) {
                res.send({
                  msg: `put an article success - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
                  putJson: updateJson,
                  status: 200
                })
              } else {
                res.send({
                  msg: `put failed by some undefined mistake - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
                  status: 400
                })
              }
            })
          })
        } else {
          res.send({
            msg: `no such id - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
            status: 401
          })
        }
      }
    }
    putArticle().catch((err) => {
      console.log(err)
      res.send({
        msg: `no rights to edit page, check your token or id - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
        status: 402
      })
    })

  })
}
