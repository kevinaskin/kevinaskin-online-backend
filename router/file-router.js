const { host, CLOUD_STATIC_PATH } = require('../config/CONFIG')
const jwt = require('jsonwebtoken')
const salt = require('../config/CONFIG').salt
const moment = require('moment')
const multer = require('multer')
const upload = multer({ dest: process.env.NODE_ENV === 'production' ? CLOUD_STATIC_PATH : './static/upload-img' })
const fs = require('fs')
const checkAuth = require('../utils/function').checkAuth
const MongoDB = require('mongodb')
const ObjectId = MongoDB.ObjectID

module.exports = (app, mongo, DB_PATH) => {

  app.delete('/api/removeImg', (req, res) => {

    let checkId = () => {
      return new Promise((resolve, reject) => {
        mongo.connect(DB_PATH, (err, db) => {
          let userFile = db.collection('userFile')
          let _id = ''
          if ((typeof req.body.id === 'string' && req.body.id.length === 24)) {
            _id = ObjectId(req.body.id)
            userFile.find({ _id: _id }).toArray((err, id) => {
              console.log('ids', id)
              if (id.length > 0) {
                resolve('next')
              } else {
                reject('reject')
              }
              db.close()
            })
          } else {
            reject('reject')
          }
        })
      })
    }

    let removeImg = async () => {
      try {
        let judgeAuth = await checkAuth(req.body, mongo, DB_PATH)
        try {
          let judgeId = await checkId()
          if (judgeAuth === 'next' && judgeId === 'next') {
            let id = req.body.id
            if (id) {
              mongo.connect(DB_PATH, (err, db) => {
                if (!err) {
                  let userFile = db.collection('userFile')
                  userFile.find({ _id: ObjectId(id) }).toArray((err, arr) => {
                    if (!err) {
                      let urlArr = arr[0].url.split('/')
                      let unlinkUrl = process.env.NODE_ENV === 'dev'
                        ? './static/upload-img/' + urlArr[2]
                        : '/home/gitlab-runner/static/upload-img/' + urlArr[2]
                      fs.unlinkSync(unlinkUrl)
                      userFile.deleteOne({ _id: ObjectId(id) }, (err, file) => {
                        if (!err) {
                          res.send({
                            msg: 'delete file success',
                            status: 200
                          })
                        }
                      })
                    }
                  })
                }
              })
            } else {
              res.send({
                msg: 'you must send an id of img',
                status: 400
              })
            }
          }
        } catch (err) {
          res.send({
            msg: 'id undefined',
            status: 401
          })
        }

      } catch (err) {
        res.send({
          msg: 'token check failed',
          status: 402
        })
      }


    }

    removeImg()
  })

  app.post('/api/uploadImg', upload.single('image'), (req, res) => {

    const file = req.file;
    let name = file.originalname;
    let nameArr = name.split('.')
    let ext = nameArr[nameArr.length - 1]

    let url = `${host}/upload-img/${file.filename}.${ext}`
    let username = ''

    jwt.verify(req.headers.authorization, salt, (err, decode) => {
      if (!err) {
        username = decode.username

        mongo.connect(DB_PATH, (err, db) => {
          if (!err) {
            let userFile = db.collection('userFile')
            userFile.find({ username }).toArray((err, arr) => {
              if (!err) {
                if (arr.length < 500) {
                  userFile.insertOne({ username, url }, (err, result) => {
                    if (!err) {
                      res.send({
                        url: url,
                        status: 200
                      })

                      let relativeSrc = process.env.NODE_ENV === 'production' ? CLOUD_STATIC_PATH : './static/upload-img/' + file.filename
                      fs.renameSync(relativeSrc, relativeSrc + '.' + ext)
                    } else {
                      res.send({
                        msg: 'save in db failed',
                        status: 400
                      })
                    }
                    db.close()
                  })
                } else {
                  res.send({
                    msg: 'you have upload too much img before',
                    status: 401
                  })
                }
              }
            })
          }
        })


      } else {
        res.send({
          msg: 'token wrong',
          status: 402
        })
      }
    })
  })
}
