const jwt = require('jsonwebtoken')
const salt = require('../config/CONFIG').salt
const moment = require('moment')

module.exports = (app, mongo, DB_PATH) => {
  app.put('/api/resetPassword', (req, res) => {

    /**
     * req.body
     *
     * {
     *    username: String,
     *    oldPassword: String,
     *    newPassword: String
     * }
     */

    let putInfo = req.body
    mongo.connect(DB_PATH, (err, db) => {
      let users = db.collection('users')
      console.log(putInfo)
      users
        .find({ username: putInfo.username, password: putInfo.oldPassword })
        .toArray((err, user) => {
          if (user.length) {
            users.updateOne({ username: putInfo.username }, { $set: { password: putInfo.newPassword } }, (err, update) => {
              if (!err) {
                console.log(update)
                console.log(`user [ ${putInfo.username} ] has changed his password just now`)
                res.send({
                  msg: `you are change your password success - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
                  status: 200
                })
              } else {
                res.send({
                  msg: `change password failed - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
                  status: 400
                })
              }
            })
          } else {
            res.send({
              msg: `you have post a wrong password or username - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
              status: 401
            })
          }
        })
    })
  })
  app.post('/api/signUp', (req, res) => {

    /**
     * req.body
     *
     * {
     *    username: String,
     *    password: String,
     *    mail: String
     * }
     */

    let signInfo = req.body
    mongo.connect(DB_PATH, (err, db) => {
      let users = db.collection('users')
      users
        .find({ username: signInfo.username })
        .toArray((err, user) => {
          if (user.length) {
            res.send({
              msg: 'this username has been signed before',
              status: 400
            })
            db.close()
          } else {
            users.insertOne(signInfo, (err, user) => {
              if (!err) {
                let token = jwt.sign({
                  username: signInfo.username,
                  password: signInfo.password,
                  timeTag: moment().format('x')
                }, salt)
                res.send({
                  msg: `sign up success - ${moment().format('YYYY/MM/DD dddd h:mm:ss a')}`,
                  content: {
                    token
                  },
                  status: 200
                })
              }
            })
          }
        })
    })
  })

  app.post('/api/login', (req, res) => {

    /**
     *  req.body
     *
     *  {
     *    username: String,
     *    password: String
     *  }
     */

    let userInfo = req.body
    mongo.connect(DB_PATH, (err, db) => {
      let users = db.collection('users')
      users
        .find(userInfo)
        .toArray((err, user) => {
          if (user.length) {
            let token = jwt.sign({
              username: userInfo.username,
              password: userInfo.password,
              timeTag: moment().format('x')
            }, salt)

            res.send({
              msg: 'login succeed',
              content: {
                username: userInfo.username,
                token
              },
              status: 200
            })
          } else {
            res.send({
              msg: 'login failed',
              status: 400
            })
          }
          db.close()
        })
    })
  })
}
