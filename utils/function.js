const jwt = require('jsonwebtoken')
const salt = require('../config/CONFIG').salt

let checkAuth = (reqBody, mongo, DB_PATH) => {
  return new Promise((resolve, reject) => {
    let decodeUsername = ''
    jwt.verify(reqBody.token, salt, (err, decode) => {
      if (!err) {
        console.log(decode)
        decodeUsername = decode.username
      }
    })
    mongo.connect(DB_PATH, (err, db) => {
      let users = db.collection('users')

      users.find({ username: decodeUsername }).toArray((err, user) => {
        if (user.length > 0) {
          resolve('next')
        } else {
          reject('reject')
        }
        db.close()
      })
    })
  })
}


module.exports = {
  checkAuth: checkAuth
}